package com.cmd;

public class PermissionsDictionaryEntry {
	
	private String permission;
	private String category;
	
	public PermissionsDictionaryEntry(String permission, String category) {
		// TODO Auto-generated constructor stub
		this.permission = permission;
		this.category = category;
	}
	 public String getCategory() {
		return category;
	}
	 
	 public String getPermission() {
		return permission;
	}
}