package com.cmd;

import java.util.HashSet;
import java.util.Set;

public class CategoryAggregateResult {
	private String category;
	private Set<String> users = new HashSet<String>();
	private Set<String> permissions = new HashSet<String>();
	private Set<String> profiles = new HashSet<String>();
	private Set<String> permissionSets = new HashSet<String>();	
	
	public String getCategory() {
		return category;
	}
	
	public Set<String> getUsers() {
		return users;
	}
	
	
	public Set<String> getPermissions() {
		return permissions;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}
	
	public void setUsers(Set<String> users) {
		this.users = users;
	}
	
	public boolean hasUsers() {
		return users != null && !users.isEmpty();
	}
	
	public Integer getUsersCnt() {
		return users == null ? 0 : users.size();
	}
	
	@Override
	public String toString() {
		return String.join(",", new String[] {category, String.valueOf(getUsersCnt()), String.join(";", users), String.join(";", permissions), String.join(";", profiles), String.join(";", permissionSets)});
	}

	public Set<String> getProfiles() {
		return profiles;
	}

	public void setProfiles(Set<String> profiles) {
		this.profiles = profiles;
	}

	public Set<String> getPermissionSets() {
		return permissionSets;
	}

	public void setPermissionSets(Set<String> permissionSets) {
		this.permissionSets = permissionSets;
	}
}
