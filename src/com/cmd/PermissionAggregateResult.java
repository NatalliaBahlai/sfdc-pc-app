package com.cmd;

import java.util.HashSet;
import java.util.Set;

public class PermissionAggregateResult {
	private String category;
	private String permission;
	private Set<String> users = new HashSet<String>();

	public String getCategory() {
		return category;
	}

	public String getPermission() {
		return permission;
	}

	public Set<String> getUsers() {
		return users;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public void setUsers(Set<String> users) {
		this.users = users;
	}

	public Integer getUsersCnt() {
		return users == null ? 0 : users.size();
	}

	@Override
	public String toString() {
		return String.join(",",
				new String[] { category, permission, String.valueOf(getUsersCnt()), String.join(";", users) });
	}
}
