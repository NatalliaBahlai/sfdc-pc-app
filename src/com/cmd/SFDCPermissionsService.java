package com.cmd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.GetUserInfoResult;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

public class SFDCPermissionsService {
	private static final String PERMISSION_SET = "PermissionSet";
	private static final String PROFILE = "Profile";

	String authEndPoint = "";
	String username = "";
	String password = "";
	PartnerConnection connection;

	public SFDCPermissionsService(String endpoint, String username, String password) {
		this.authEndPoint = endpoint + "/services/Soap/u/36.0";
		this.username = username;
		this.password = password;
	}

	public boolean login() {
		boolean success = false;
		String username = this.username;
		String password = this.password;

		try {
			ConnectorConfig config = new ConnectorConfig();
			config.setUsername(username);
			config.setPassword(password);
			config.setAuthEndpoint(authEndPoint);

			connection = new PartnerConnection(config);
			printUserInfo(config);

			success = true;
		} catch (ConnectionException ce) {
			ce.printStackTrace();
		}
		return success;
	}

	public void printUserInfo(ConnectorConfig config) {
		try {
			GetUserInfoResult userInfo = connection.getUserInfo();

			System.out.println("\nLogging in ...\n");
			System.out.println("UserID: " + userInfo.getUserId());
			System.out.println("User Full Name: " + userInfo.getUserFullName());
			System.out.println("User Email: " + userInfo.getUserEmail());
			System.out.println();
		} catch (ConnectionException ce) {
			ce.printStackTrace();
		}
	}

	public void logout() {
		try {
			connection.logout();
			System.out.println("Logged out.");
		} catch (ConnectionException ce) {
			ce.printStackTrace();
		}
	}

	private Map<String, List<String>> collectPermissionsFlds(String[] objs) {

		try {
			DescribeSObjectResult[] res = connection.describeSObjects(objs);

			Map<String, List<String>> result = new HashMap<String, List<String>>();
			for (DescribeSObjectResult r : res) {
				List<String> fields = new ArrayList<String>();
				result.put(r.getName(), fields);
				for (com.sforce.soap.partner.Field fld : r.getFields()) {
					if (fld.getName().startsWith("Permissions"))
						fields.add(fld.getName());
				}
			}
			return result;
		} catch (ConnectionException e) {
			e.printStackTrace();
		}

		return null;
	}

	private Map<String, List<String>> readDictionary() {
		InputStream in = this.getClass().getResourceAsStream("/com/resources/permissionsandcategories.csv");
		BufferedReader breader = new BufferedReader(new InputStreamReader(in));

		List<String> lines = breader.lines().collect(Collectors.toList());
		Map<String, List<String>> dictionary = lines.stream().skip(1).map(list -> {
			String[] p = list.split(",");
			return new PermissionsDictionaryEntry(p[0], p[1]);
		}).collect(Collectors.groupingBy(PermissionsDictionaryEntry::getCategory,
				Collectors.mapping(PermissionsDictionaryEntry::getPermission, Collectors.toList())));

		return dictionary;
	}

	private Map<String, List<String>> processPermissionSets(List<String> fields)
			throws ConnectionException, IllegalArgumentException, IllegalAccessException {
		String flds = fields.stream().map(e -> {
			e = PERMISSION_SET + "." + e;
			return e;
		}).collect(Collectors.joining(","));
		String soqlQuery = "SELECT Assignee.Name, " + flds + " FROM PermissionSetAssignment WHERE Assignee.isActive = true  and PermissionSet.IsOwnedByProfile = False";//

		QueryResult qr = connection.queryAll(soqlQuery);

		Map<String, List<String>> psSnapshot = new HashMap<String, List<String>>();
		for (String p : fields) {
			psSnapshot.put(p, new ArrayList<String>());
				
			for (SObject o : qr.getRecords()) {
				SObject ps = (SObject) o.getField("PermissionSet");
				SObject assignee = (SObject) o.getField("Assignee");
				String fld = (String) ps.getField(p);
				String username = (String) assignee.getField("Name");
				if (Boolean.valueOf(fld) && username != null) psSnapshot.get(p).add(username);
				
				
				/* Enterprize.wsdl
				PermissionSetAssignment psa = (PermissionSetAssignment) o;
				try {
					Field fld = PermissionSet.class.getDeclaredField(p);
					fld.setAccessible(true);
					Boolean val = (Boolean) fld.get(psa.getPermissionSet());
					if (val)
						psSnapshot.get(p).add(psa.getAssignee().getName());

				} catch (NoSuchFieldException ex) {
					ex.printStackTrace();
				}
			*/
			}
		}

		return psSnapshot;
	}

	private Map<String, List<String>> processProfiles(List<String> fields)
			throws ConnectionException, IllegalArgumentException, IllegalAccessException {
		String flds = fields.stream().map(e -> {
			e = PROFILE + "." + e;
			return e;
		}).collect(Collectors.joining(","));
		String soqlQuery = "SELECT Name, " + flds + " FROM User  WHERE isActive = true";//

		QueryResult qr = connection.queryAll(soqlQuery);

		Map<String, List<String>> profileSnapshot = new HashMap<String, List<String>>();
		for (String p : fields) {
			profileSnapshot.put(p, new ArrayList<String>());

			for (SObject o : qr.getRecords()) {

				SObject profile = (SObject) o.getField("Profile");
				String fld = (String) profile.getField(p);
				String username = (String) o.getField("Name");
				if (Boolean.valueOf(fld) && username != null) profileSnapshot.get(p).add(username);
				
				/* Enterprize.wsdl
				User u = (User) o;
				try {
					Field fld = Profile.class.getDeclaredField(p);
					fld.setAccessible(true);
					Boolean val = (Boolean) fld.get(u.getProfile());
					if (val)
						profileSnapshot.get(p).add(u.getName());

				} catch (NoSuchFieldException ex) {
					ex.printStackTrace();
				}*/
			}

		}

		return profileSnapshot;
	}
	
	private Map<String, List<String>> processProfileNames(List<String> fields)
			throws ConnectionException, IllegalArgumentException, IllegalAccessException {
		String flds = fields.stream().map(e -> {
			e = PROFILE + "." + e;
			return e;
		}).collect(Collectors.joining(","));
		String soqlQuery = "SELECT Name, Profile.Name, " + flds + " FROM User  WHERE isActive = true";//

		QueryResult qr = connection.queryAll(soqlQuery);

		Map<String, List<String>> profileSnapshot = new HashMap<String, List<String>>();
		for (String p : fields) {
			profileSnapshot.put(p, new ArrayList<String>());

			for (SObject o : qr.getRecords()) {

				SObject profile = (SObject) o.getField("Profile");
				String fld = (String) profile.getField(p);
				String profileName = (String) profile.getField("Name");
				if (Boolean.valueOf(fld) && username != null) profileSnapshot.get(p).add(profileName);
				
			
			}

		}

		return profileSnapshot;
	}

	private Map<String, List<String>> processPermissionSetNames(List<String> fields)
			throws ConnectionException, IllegalArgumentException, IllegalAccessException {
		String flds = fields.stream().map(e -> {
			e = PERMISSION_SET + "." + e;
			return e;
		}).collect(Collectors.joining(","));
		String soqlQuery = "SELECT PermissionSet.Name, Assignee.Name, " + flds + " FROM PermissionSetAssignment WHERE Assignee.isActive = true and PermissionSet.IsOwnedByProfile = False";//

		QueryResult qr = connection.queryAll(soqlQuery);

		Map<String, List<String>> psSnapshot = new HashMap<String, List<String>>();
		for (String p : fields) {
			psSnapshot.put(p, new ArrayList<String>());
				
			for (SObject o : qr.getRecords()) {
				SObject ps = (SObject) o.getField("PermissionSet");
				String name = (String) ps.getField("Name");
				String fld = (String) ps.getField(p);

				if (Boolean.valueOf(fld)) psSnapshot.get(p).add(name);
				
				
			}
		}

		return psSnapshot;
	}
	
	public <T> void execute() throws NoSuchFieldException, SecurityException, IllegalArgumentException,
			IllegalAccessException, IOException, ConnectionException {

		Map<String, List<String>> permissionsByCategory = readDictionary();

		Map<String, List<String>> fieldsByObject = collectPermissionsFlds(new String[] { PROFILE, PERMISSION_SET });
		Map<String, List<String>> usersByPermission = processProfiles(fieldsByObject.get(PROFILE));
		Map<String, List<String>> profilesByPermission = processProfileNames(fieldsByObject.get(PROFILE));
		Map<String, List<String>> psSnapshot = processPermissionSets(fieldsByObject.get(PERMISSION_SET));
		Map<String, List<String>> permissionSetsByPermission = processPermissionSetNames(fieldsByObject.get(PERMISSION_SET));

		List<CategoryAggregateResult> result = new ArrayList<CategoryAggregateResult>();
		for (String categoryName : permissionsByCategory.keySet()) {
			CategoryAggregateResult ac = new CategoryAggregateResult();
			ac.setCategory(categoryName);

			for (String permission : permissionsByCategory.get(categoryName)) {
				ac.getPermissions().add(permission);
				if (usersByPermission.containsKey(permission))
					ac.getUsers().addAll(usersByPermission.get(permission));
				
				if (profilesByPermission.containsKey(permission)) {
					ac.getProfiles().addAll(profilesByPermission.get(permission));
				}

				if (psSnapshot.containsKey(permission))
					ac.getUsers().addAll(psSnapshot.get(permission));

				if (permissionSetsByPermission.containsKey(permission)) {
					ac.getPermissionSets().addAll(permissionSetsByPermission.get(permission));
				}
			}
			result.add(ac);
		}

		writeCSV(result);
	}
	
	public void writeCSV(List<CategoryAggregateResult> result) throws IOException {
		String filename = username + " " + new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date()) + ".csv";
		File file = new File(".", filename);

		if (!file.exists())
			file.createNewFile();

		BufferedWriter output = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(file, false), StandardCharsets.UTF_8));
		try {
			output.write("CATEGORY, USERS_COUNT, USERS, PERMISSIONS, PROFILES, PERMISSIONSETS");
			output.newLine();
			for (CategoryAggregateResult ac : result) {
				if (ac.hasUsers()) {
					output.write(ac.toString());
					output.newLine();
				}
			}
			System.out.println("Please check the file: " + file.getCanonicalPath());
		} finally {
			output.close();
		}
	}
}
