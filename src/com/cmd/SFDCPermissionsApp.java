package com.cmd;

import java.io.IOException;

import com.sforce.ws.ConnectionException;

public class SFDCPermissionsApp {

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, IOException, ConnectionException {
		if (args.length < 3) {
			System.out.println("Please provide parameters: endpoint username password+token");

			System.exit(-1);
		}

		execute(args[0], args[1], args[2]);
	}

	public static void execute(String endpoint, String username, String password) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, IOException, ConnectionException {
		SFDCPermissionsService service = new SFDCPermissionsService(endpoint, username, password);
		
		if (service.login()) {

			service.execute();

			service.logout();
		}
	}
}