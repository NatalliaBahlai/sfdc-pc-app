# README #


### What is this repository for? ###

* The java app groups user permissions ( extracted from Profile and Permission Sets of Salesforce org) by category
* Find categorization [here](https://bitbucket.org/NatalliaBahlai/sfdc-pc-app/src/16d21a6e0863d8200d20a39dc6b03c4d570a548d/src/com/resources/permissionsandcategories.csv?at=master)
* [sfdc-pc-app.jar](https://bitbucket.org/NatalliaBahlai/sfdc-pc-app/downloads/sfdc-pc-app.jar) calculates permissions for active users
* [sfdc-pc-all-app.jar](https://bitbucket.org/NatalliaBahlai/sfdc-pc-app/downloads/sfdc-pc-all-app.jar) calculates permissions for all users

### How do I use it? ###

* Download [sfdc-pc-app.jar](https://bitbucket.org/NatalliaBahlai/sfdc-pc-app/downloads/sfdc-pc-app.jar)
* Open command line and execute:

```
#!java

java -jar sfdc-pc-app.jar https://test.salesforce.com username@company.com password+securitytoken
```